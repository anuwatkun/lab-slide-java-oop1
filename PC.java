package oop_java;

public class PC implements IDComputer {

	public String title;
	public int idcom;
	
	public PC(String titleInput, int idcomInput) {
		this.idcom = idcomInput;
		this.title = titleInput;
	}

	@Override
	public String getTitlePc() {
		
		return title;
	}

	@Override
	public int getIdPc() {
		
		return idcom;
	}

	@Override
	public String toString() {
		
		return " ID :" + idcom + "  |  " + title;
	}
	
}